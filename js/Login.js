$(document).on("ready", inicio);

function inicio(){

	$("span.help-block").hide();
	$("#btn-validar-Login").click(validarUsernameLogin);
	$("#btn-validar-Login").click(validarPasswordLogin);

	$("#userName").keyup(validarUsernameLogin);
	$("#password-login").keyup(validarPasswordLogin);
}

//Se procede a validar todos los inputs
function validarUsernameLogin(){
	var username = document.getElementById("userName").value;
	var expRegular = /^[A-Za-z0-9\_\-\*\+\$\#\@\&\xF1\xD1]+$/;
	if( username == null || username.length == 0 || /^\s+$/.test(username) || username == "" ) {
		$("#iconotexto").remove();
		$("#userName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName").parent().children("span").text("El usuario es requerido").show();
		return false;
	}else if (!expRegular.test(username)) {
		$("#iconotexto").remove();
		$("#userName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#iconotexto").remove();
		$("#userName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#userName").parent().children("span").text("").show();
		return true;
	}
}
function validarPasswordLogin(){
	var password = document.getElementById("password-login").value;
	var expRegular = /^[A-Za-z0-9]+$/;
	if( password == null || password.length == 0 || /^\s+$/.test(password) || password == "" ) {
		$("#iconotexto").remove();
		$("#password-login").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password-login").parent().children("span").text("La contrasena es requerida").show();
		return false;
	}else if (!expRegular.test(password)) {
		$("#iconotexto").remove();
		$("#password-login").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password-login").parent().children("span").text("Solo puede ingresar letras, espacios en blanco y numeros").show();
		return false;
	}else{
		$("#iconotexto").remove();
		$("#password-login").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#password-login").parent().children("span").text("").show();
		return true;
	}
}

//Se van a recibir los datos, y luego se validan y si son correctos retorna un true
//la información obtenida se va a agregar al key en localStorage
ClientLog = function  (userName, password) {
	this.userName = userName;
	this.password = password;

	this.foundUser = function() {
		var users = [JSON.parse(localStorage["Users"])];
		var array = [];
		var found = false;

		for (var i = users.length - 1; i >= 0; i--) {
			array = users[i];
			for (var i = array.length - 1; i >= 0; i--) {
				var object = array[i]
				if (object.userName == userName
					&& object.password == password) {
					found = true;

				var arayUserLogin = [];
				arayUserLogin[0] = object.userName;
				arayUserLogin[1] = object.phoneNumber
				localStorage["user-log"] = JSON.stringify(arayUserLogin);
			}
		}
	}
	return found;
};
},

LoginEnter = function (){
	var userName = document.getElementById('userName').value;
	var password = document.getElementById('password-login').value;

	if (validarUsernameLogin() && validarPasswordLogin()) {
		//Se crea una instancia y se envian los datos por parametros.
		var client1 = new ClientLog(userName, password);
		client1.foundUser();
		if (client1.foundUser()) {
			document.getElementById('userName').value = "";
			document.getElementById('password-login').value = "";
			document.getElementById('password-login').focus();
			alert("Datos correctos");
			//y si no se dirige hacia otra pagina
			document.formulario.submit();
		}else{

			document.getElementById('userName').value = "";
			document.getElementById('password-login').value = "";
			document.getElementById('password-login').focus();

		}
	}
}
