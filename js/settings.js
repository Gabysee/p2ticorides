$(document).on("ready", inicio);

function inicio(){
	$("span.help-block").hide();
	$("#btnvalidar").click(validarPhone);
	$("#btnvalidar").click(validarName);
	$("#btnvalidar").click(validarLastName);
	$("#btnvalidar").click(validarUserName);
	$("#btnvalidar").click(validarPassword);
	$("#btnvalidar").click(validarRepeatPassword);

	$("#phone").keyup(validarPhone);
	$("#firstName").keyup(validarName);
	$("#lastName").keyup(validarLastName);
	$("#userName").keyup(validarUserName);
	$("#password").keyup(validarPassword);
	$("#repeat-password").keyup(validarRepeatPassword);
}

//Validaciones para los input.
function validarPhone(){
	var phone = document.getElementById("phone").value;
	if( phone == null || phone.length == 0 || /^\s+$/.test(phone) ) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("El numero de telefono es requerido").show();
		return false;
	}else if (isNaN(phone)) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Debe ingresar valores numericos").show();
		return false;
	}else if ( phone.length > 8) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Son ocho digitos").show();
		return false;
	}else if ( phone.length < 8) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Son ocho digitos").show();
		return false;
	}else{
		$("#phone").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#phone").parent().children("span").text("").show();
		return true;
	}
}
function validarName(){
	var name = document.getElementById("firstName").value;
	var expRegular = /^[A-Za-z\s\xF1\xD1]+$/;
	if( name == null || name.length == 0 || /^\s+$/.test(name) || name == "" ) {
		$("#firstName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#firstName").parent().children("span").text("El campo nombre es requerido").show();
		return false;
	}else if (!expRegular.test(name)) {
		$("#firstName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#firstName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#firstName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#firstName").parent().children("span").text("").show();
		return true;
	}
}
function validarLastName(){
	var lastname = document.getElementById("lastName").value;
	var expRegular = /^[A-Za-z\s\xF1\xD1]+$/;
	if( lastname == null || lastname.length == 0 || /^\s+$/.test(lastname) || lastname == "" ) {
		$("#lastName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#lastName").parent().children("span").text("El campo apellido es requerido").show();
		return false;
	}else if (!expRegular.test(lastname)) {
		$("#lastName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#lastName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#lastName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#lastName").parent().children("span").text("").show();
		return true;
	}
}
function validarUserName(){
	var username = document.getElementById("userName").value;
	var expRegular = /^[A-Za-z0-9\_\-\*\+\$\#\@\&\xF1\xD1]+$/;
	if( username == null || username.length == 0 || /^\s+$/.test(username) || username == "" ) {
		$("#userName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName").parent().children("span").text("El campo nombre usuario es requerido").show();
		return false;
	}else if (!expRegular.test(username)) {
		$("#userName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#userName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#userName").parent().children("span").text("").show();
		return true;
	}
}
function validarPassword(){
	var password = document.getElementById("password").value;
	var expRegular = /^[A-Za-z0-9]+$/;
	if( password == null || password.length == 0 || /^\s+$/.test(password) || password == "" ) {
		$("#password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password").parent().children("span").text("La contrasena es requerido").show();
		return false;
	}else if (!expRegular.test(password)) {
		$("#password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}
	else if (password.length < 8) {
		$("#password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password").parent().children("span").text("Debe llevar minimo ocho caracteres").show();
		return false;
	}else{
		$("#password").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#password").parent().children("span").text("").show();
		return true;
	}
}
function validarRepeatPassword(){
	var repeatPassword = document.getElementById("repeat-password").value;
	var expRegular = /^[A-Za-z0-9]+$/;
	if( repeatPassword == null || repeatPassword.length == 0 || /^\s+$/.test(repeatPassword) || repeatPassword == "" ) {
		$("#repeat-password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password").parent().children("span").text("Este campo es requerido").show();
		return false;
	}else if (!expRegular.test(repeatPassword)) {
		$("#repeat-password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else if (repeatPassword.length < 8) {
		$("#repeat-password").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password").parent().children("span").text("Debe llevar minimo ocho caracteres").show();
		return false;
	}else{
		$("#repeat-password").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#repeat-password").parent().children("span").text("").show();
		return true;
	}
}


var phone;
var position;
var RIDES = {
	property: 10,

	initialize: function() {
		RIDES.userData();
		RIDES.editUser();
	},

	//Carga el usuario y el telefoo y validas los rides
	userData: function(){
		var user = [];
		user = JSON.parse(localStorage.getItem('user-log'));
		var object = user[0];
		phone = user[1];
		document.getElementById("user-name").innerHTML = object;

	},

	//Setea los valores que esten en el localStorage y valida cual Usuario se va a editar.
	editUser: function(){
		var user = [JSON.parse(localStorage.getItem('Users'))];
		var userArray = [];
		for (var i = user.length - 1; i >= 0; i--) {
			userArray = user[i];
			for (var i = userArray.length - 1; i >= 0; i--) {
				var object = userArray[i]
				if (phone == object.phoneNumber) {
					position = i;
					document.getElementById('firstName').value = object.firstName;
					document.getElementById('lastName').value = object.lastName;
					document.getElementById('phone').value = object.phoneNumber;
					document.getElementById('userName').value = object.userName;
					document.getElementById('password').value = object.password;
				}
			}
		}
	},
};
function rideEditSave(){
	var firstName = document.getElementById('firstName').value;
	var lastName = document.getElementById('lastName').value;
	var phoneNumber = document.getElementById('phone').value;
	var userName = document.getElementById('userName').value;
	var password = document.getElementById('password').value;
	var repeatPassword = document.getElementById('repeat-password').value;

	var editRide = {
		firstName,
		lastName,
		phoneNumber,
		userName,
		password
	};

	if (validarName() && validarLastName()
		&& validarPhone() && validarUserName()
		&& validarPassword() && validarRepeatPassword()) {
		if (password == repeatPassword) {
			var userArrayBackup = [];
			var usersSave = [JSON.parse(localStorage.getItem('Users'))];
			//Recorrer los valores que esten en el localStorage
			for (var i = usersSave.length - 1; i >= 0; i--) {
				array = usersSave[i];
				for (var i = array.length - 1; i >= 0; i--) {
					if (position == i) {
						//Agregar el objeto editado a la posicion donde estaba anteriormente.
						userArrayBackup[position] = editRide;
						alert("Perfil editado");
					}else{
						var object =  array[i];
						userArrayBackup[i] = object;
					}
				}
			}
			//Agregar al localStorage
			localStorage["Users"] = JSON.stringify(userArrayBackup);
		}else{
			alert("Contrasenas incorrectas");
			document.getElementById('repeat-password').value = "";
			document.getElementById('repeat-password').focus();
		}
	}
}
RIDES.initialize();
