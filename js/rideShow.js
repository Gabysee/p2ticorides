var RIDES = {
	property: 10,

	initialize: function() {
		RIDES.loadUsers();
		hide();
	},
	loadUsers: function() {

      //leer de localStorage los usuarios
      var rides = [];
      if (localStorage.getItem('rides')) {
      	rides = JSON.parse(localStorage.getItem('rides'));
      }
      //agregar cada usuario al DOM
      rides.forEach(function(rides, index, users) {
      	RIDES.addUser(rides);
      });

    },
    addUser: function(rides) {
      //Obtener dias nuevos dias que se han editado.
      var days = "";
      for (var i=0; rides.days[i]; ++i) {
        var day = rides.days[i];
        switch (day) {
          case "Monday":
          days += day;
          break;
          case "Tuesday":
          days += day;
          break;
          case "Wednesday":
          days += day;
          break;
          case "Thursday":
          days += day;
          break;
          case "Friday":
          days += day;
          break;
          case "Saturday":
          days += day;
          break;
          case "Sunday":
          days += day;
          break;
        }
      }
      var userAll = [JSON.parse(localStorage.getItem('Users'))];
      //Obtener el nombre del usuario
      for (var i = userAll.length - 1; i >= 0; i--) {
       var userObject = userAll[i]
       for (var i = userObject.length - 1; i >= 0; i--) {
         var object = userObject[i]
         if (rides.phone == object.phoneNumber) {

              // crear una HTML fila
              var row = "<tr><td>"+rides.rideName+"</td><td>"+rides.startFrom+"</td><td>"+rides.end+
              "</td><td>"+"<a href='#' onclick='view(this.parentNode.parentNode.rowIndex)' data-toggle='modal' data-target='#rides'>view</a>"+
              "</td><td>"+rides.departure+"</td><td>"+rides.arrival+"</td><td>"+rides.phone+"</td><td>"+object.firstName+"</td><td>"+days+"</td></tr>";
            }
          }
        }

		// agregar a la tabla
		var table = document.getElementById("tb_rides");
		table.innerHTML = table.innerHTML + row;

  },
};
function hide() {
  //Obtener informacion de la fila seleccionada y luego crear un objeto con dicha informacion.
  document.getElementsByTagName("table")[0].setAttribute("id","tableid");
  //la función recibe como parámetros el numero de la columna a ocultar
  var fila = document.getElementById("tableid").getElementsByTagName('tr');

   //mostramos u ocultamos la cabecera de la columna
   if (fila[0].getElementsByTagName('th')[4].style.display=='none')  {
    fila[0].getElementsByTagName('th')[4].style.display=''
  }else  {
    fila[0].getElementsByTagName('th')[4].style.display='none'
  }

  for(i=1;i<fila.length;i++)  {
    if (fila[i].getElementsByTagName('td')[4].style.display=='none')    {
      fila[i].getElementsByTagName('td')[4].style.display='';
    }else{
      fila[i].getElementsByTagName('td')[4].style.display='none'
    }
  }
  if (fila[0].getElementsByTagName('th')[5].style.display=='none')  {
    fila[0].getElementsByTagName('th')[5].style.display=''
  }else{
    fila[0].getElementsByTagName('th')[5].style.display='none'
  }
  for(i=1;i<fila.length;i++)  {
    if (fila[i].getElementsByTagName('td')[5].style.display=='none')    {
      fila[i].getElementsByTagName('td')[5].style.display='';
    }else{
      fila[i].getElementsByTagName('td')[5].style.display='none'
    }
  }
  if (fila[0].getElementsByTagName('th')[6].style.display=='none')  {
    fila[0].getElementsByTagName('th')[6].style.display=''
  }else{
    fila[0].getElementsByTagName('th')[6].style.display='none'
  }
  for(i=1;i<fila.length;i++)  {
    if (fila[i].getElementsByTagName('td')[6].style.display=='none')    {
      fila[i].getElementsByTagName('td')[6].style.display='';
    }else{
      fila[i].getElementsByTagName('td')[6].style.display='none'
    }
  }
  if (fila[0].getElementsByTagName('th')[7].style.display=='none')  {
    fila[0].getElementsByTagName('th')[7].style.display=''
  }else{
    fila[0].getElementsByTagName('th')[7].style.display='none'
  }
  for(i=1;i<fila.length;i++)  {
    if (fila[i].getElementsByTagName('td')[7].style.display=='none')    {
      fila[i].getElementsByTagName('td')[7].style.display='';
    }else{
      fila[i].getElementsByTagName('td')[7].style.display='none'
    }
  }
  if (fila[0].getElementsByTagName('th')[8].style.display=='none')  {
    fila[0].getElementsByTagName('th')[8].style.display=''
  }else{
    fila[0].getElementsByTagName('th')[8].style.display='none'
  }
  for(i=1;i<fila.length;i++)  {
    if (fila[i].getElementsByTagName('td')[8].style.display=='none')    {
      fila[i].getElementsByTagName('td')[8].style.display='';
    }else{
      fila[i].getElementsByTagName('td')[8].style.display='none'
    }
  }
}

function view(j){
  document.getElementsByTagName("table")[0].setAttribute("id","tableid");

  //Almacenar los datos de cada columna en una variable.
  var rideName = document.getElementById("tableid").rows[j].cells[0].innerHTML;
  var start = document.getElementById("tableid").rows[j].cells[1].innerHTML;
  var end = document.getElementById("tableid").rows[j].cells[2].innerHTML;
  var departure = document.getElementById("tableid").rows[j].cells[4].innerHTML;
  var arrival = document.getElementById("tableid").rows[j].cells[5].innerHTML;
  var phone = document.getElementById("tableid").rows[j].cells[6].innerHTML;
  var firstName = document.getElementById("tableid").rows[j].cells[7].innerHTML;
  var dayShow = document.getElementById("tableid").rows[j].cells[8].innerHTML;

  //Crear objeto con los valores de la fila seleccionada.
  var rideShow = {
    rideName,
    start,
    end,
    departure,
    arrival,
    phone,
    firstName,
    dayShow
  };

  //Mostrar todos los datos de cada ride, y setear la informacion para mostrarla.
  document.getElementById('phonenumber').innerHTML = rideShow.phone;
  document.getElementById('nameRide').innerHTML = rideShow.rideName;
  document.getElementById('start').innerHTML = rideShow.start;
  document.getElementById('end').innerHTML = rideShow.end;
  document.getElementById('departure').innerHTML = rideShow.departure;
  document.getElementById('arrival').innerHTML = rideShow.arrival;
  document.getElementById('nameUser').innerHTML = rideShow.firstName;
  document.getElementById('days').innerHTML = rideShow.dayShow;
}

RIDES.initialize();
