$(document).on("ready", inicio);

function inicio(){
	$("span.help-block").hide();
	$("#btnvalidar").click(validarPhone);
	$("#btnvalidar").click(validarName);
	$("#btnvalidar").click(validarLastName);
	$("#btnvalidar").click(validarUserName);
	$("#btnvalidar").click(validarPassword);
	$("#btnvalidar").click(validarRepeatPassword);

	$("#phone").keyup(validarPhone);
	$("#firstName").keyup(validarName);
	$("#lastName").keyup(validarLastName);
	$("#userName-register").keyup(validarUserName);
	$("#password-register").keyup(validarPassword);
	$("#repeat-password-register").keyup(validarRepeatPassword);
}
//Validación inputs.
function validarPhone(){
	var phone = document.getElementById("phone").value;
	if( phone == null || phone.length == 0 || /^\s+$/.test(phone) ) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("El numero de telefono es requerido").show();
		return false;
	}else if (isNaN(phone)) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Debe ingresar valores numericos").show();
		return false;
	}else if ( phone.length > 8) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Son ocho digitos").show();
		return false;
	}else if ( phone.length < 8) {
		$("#phone").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#phone").parent().children("span").text("Son ocho digitos").show();
		return false;
	}else{
		$("#phone").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#phone").parent().children("span").text("").show();
		return true;
	}
}
function validarName(){
	var name = document.getElementById("firstName").value;
	var expRegular = /^[A-Za-z\s\xF1\xD1]+$/;
	if( name == null || name.length == 0 || /^\s+$/.test(name) || name == "" ) {
		$("#firstName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#firstName").parent().children("span").text("El campo nombre es requerido").show();
		return false;
	}else if (!expRegular.test(name)) {
		$("#firstName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#firstName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#firstName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#firstName").parent().children("span").text("").show();
		return true;
	}
}
function validarLastName(){
	var lastname = document.getElementById("lastName").value;
	var expRegular = /^[A-Za-z\s\xF1\xD1]+$/;
	if( lastname == null || lastname.length == 0 || /^\s+$/.test(lastname) || lastname == "" ) {
		$("#lastName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#lastName").parent().children("span").text("El campo apellido es requerido").show();
		return false;
	}else if (!expRegular.test(lastname)) {
		$("#lastName").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#lastName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#lastName").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#lastName").parent().children("span").text("").show();
		return true;
	}
}
function validarUserName(){
	var username = document.getElementById("userName-register").value;
	var expRegular = /^[A-Za-z0-9\_\-\*\+\$\#\@\&\xF1\xD1]+$/;
	if( username == null || username.length == 0 || /^\s+$/.test(username) || username == "" ) {
		$("#userName-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName-register").parent().children("span").text("El campo nombre usuario es requerido").show();
		return false;
	}else if (!expRegular.test(username)) {
		$("#userName-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#userName-register").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#userName-register").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#userName-register").parent().children("span").text("").show();
		return true;
	}
}
function validarPassword(){
	var password = document.getElementById("password-register").value;
	var expRegular = /^[A-Za-z0-9]+$/;
	if( password == null || password.length == 0 || /^\s+$/.test(password) || password == "" ) {
		$("#password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password-register").parent().children("span").text("La contrasena es requerido").show();
		return false;
	}else if (!expRegular.test(password)) {
		$("#password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password-register").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}
	else if (password.length < 5) {
		$("#password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#password-register").parent().children("span").text("Mínimo cinco caracteres").show();
		return false;
	}else{
		$("#password-register").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#password-register").parent().children("span").text("").show();
		return true;
	}
}
function validarRepeatPassword(){
	var repeatPassword = document.getElementById("repeat-password-register").value;
	var expRegular = /^[A-Za-z0-9]+$/;
	if( repeatPassword == null || repeatPassword.length == 0 || /^\s+$/.test(repeatPassword) || repeatPassword == "" ) {
		$("#repeat-password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password-register").parent().children("span").text("Este campo es requerido").show();
		return false;
	}else if (!expRegular.test(repeatPassword)) {
		$("#repeat-password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password-register").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else if (repeatPassword.length < 5) {
		$("#repeat-password-register").parent().parent().attr("class", "form-group has-error has-feedback");
		$("#repeat-password-register").parent().children("span").text("Mínimo cinco caracteres").show();
		return false;
	}else{
		$("#repeat-password-register").parent().parent().attr("class", "form-group has-success has-feedback");
		$("#repeat-password-register").parent().children("span").text("").show();
		return true;
	}
}

//Luego de que se valida la información, es utilizada en la siguiente función
Client = function  (firstName, lastName, phoneNumber, userName, password) {

	this.firstName = firstName;
	this.lastName = lastName;
	this.phoneNumber = phoneNumber;
	this.userName = userName;
	this.password = password;

	//Se guardan los ususarios con su respectiva información y para esto se va a crear un objeto persona
	this.save = function() {
		var persona = {
			firstName,
			lastName,
			phoneNumber,
			userName,
			password
		};

		//Los datos se almacenan en el localStorage.
		var arregloUsers = JSON.parse(localStorage.getItem('Users'));
		if (arregloUsers == null) {
			arregloUsers = [persona];
			localStorage["Users"] = JSON.stringify(arregloUsers);
			alert("Registro exitoso");
		}else{
			arregloUsers.push(persona);
			localStorage["Users"] = JSON.stringify(arregloUsers);
			alert("REGISTRADO");
		}
	};
},

saveClient = function () {

	var firstName = document.getElementById('firstName').value;
	var lastName = document.getElementById('lastName').value;
	var phoneNumber = document.getElementById('phone').value;
	var userNameRegister = document.getElementById('userName-register').value;
	var passwordRegister = document.getElementById('password-register').value;
	var repeatPasswordRegister = document.getElementById('repeat-password-register').value;

	if (validarName() && validarLastName() && validarPhone() && validarUserName() && validarPassword() && validarRepeatPassword()) {

	if (passwordRegister == repeatPasswordRegister) {
			//Se crea una instancia y se envian los datos por parametros.
			var client1 = new Client(firstName, lastName, phoneNumber, userNameRegister, passwordRegister);
			client1.save();
			//mediante la función save se guarda al usuario
			document.getElementById('firstName').value = "";
			document.getElementById('lastName').value = "";
			document.getElementById('phone').value = "";
			document.getElementById('userName-register').value = "";
			document.getElementById('password-register').value = "";
			document.getElementById('repeat-password-register').value = "";
			document.getElementById('repeat-password-register').focus();
		}else{
			alert("Las contraseñas deben ser iguales");
		}
	}else{

		document.getElementById('firstName').value = "";
		document.getElementById('lastName').value = "";
		document.getElementById('phone').value = "";
		document.getElementById('userName-register').value = "";
		document.getElementById('password-register').value = "";
		document.getElementById('repeat-password-register').value = "";
		document.getElementById('repeat-password-register').focus();
	}
}
