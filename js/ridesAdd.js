$(document).on("ready", inicio);

var phone;
function userDatas(){
	var user = [];
	user = JSON.parse(localStorage.getItem('user-log'));
	var object = user[0];
	phone = user[1];
	document.getElementById("user-name").innerHTML = object;

}

function inicio(){
	userDatas();

	$("span.help-block").hide();
	$("#btnvalidar").click(validarRideName);
	$("#btnvalidar").click(validarStartFrom);
	$("#btnvalidar").click(validarEnd);
	$("#btnvalidar").click(validarDescription);
	$("#btnvalidar").click(validarDeparture);
	$("#btnvalidar").click(validarArrival);
	$("#btnvalidar").click(validarCheckbox);

	$("#rideName").keyup(validarRideName);
	$("#startFrom").keyup(validarStartFrom);
	$("#end").keyup(validarEnd);
	$("#description").keyup(validarDescription);
	$("#departure").keyup(validarDeparture);
	$("#arrival").keyup(validarArrival);
}
function validarRideName(){
	var rideName = document.getElementById("rideName").value;
	var expRegular = /^[A-Za-z0-9\s]+$/;
	if( rideName == null || rideName.length == 0 || /^\s+$/.test(rideName) || rideName == "" ) {
		$("#rideName").parent().children("span").text("El nombre del ride es requerido").show();
		return false;
	}else if (!expRegular.test(rideName)) {
		$("#rideName").parent().children("span").text("Solo puede ingresar letras y espacios en blanco").show();
		return false;
	}else{
		$("#rideName").parent().children("span").text("").show();
		return true;
	}
}
function validarStartFrom(){
	var startFrom = document.getElementById("startFrom").value;
	if( startFrom == null || startFrom.length == 0 || /^\s+$/.test(startFrom) || startFrom == "" ) {
		$("#startFrom").parent().children("span").text("Este campo es requerido").show();
		return false;
	}else{
		$("#startFrom").parent().children("span").text("").show();
		return true;
	}
}
function validarEnd(){
	var end = document.getElementById("end").value;
	if( end == null || end.length == 0 || /^\s+$/.test(end) || end == "" ) {
		$("#end").parent().children("span").text("Este campo es requerido").show();
		return false;
	}else{
		$("#end").parent().children("span").text("").show();
		return true;
	}
}
function validarDescription(){
	var description = document.getElementById("description").value;
	if( description == null || description.length == 0 || /^\s+$/.test(description) || description == "" ) {
		$("#description").parent().children("span").text("La descripcion es requerido").show();
		return false;
	}else{
		$("#description").parent().children("span").text("").show();
		return true;
	}
}
function validarDeparture(){
	var departure = document.getElementById("departure").value;
	var expRegular = /^[A-Za-z\s]+$/;
	if( departure == null || departure.length == 0 || /^\s+$/.test(departure) || departure == "" ) {
		$("#departure").parent().children("span").text("La hora de salida es requerida").show();
		return false;
	}else{
		$("#departure").parent().children("span").text("").show();
		return true;
	}
}
function validarArrival(){
	var arrival = document.getElementById("arrival").value;
	var expRegular = /^[A-Za-z\s]+$/;
	if( arrival == null || arrival.length == 0 || /^\s+$/.test(arrival) || arrival == "" ) {
		$("#arrival").parent().children("span").text("La hora de llegada es requerida").show();
		return false;
	}else{
		$("#arrival").parent().children("span").text("").show();
		return true;
	}

}

function validarCheckbox(){
	if(!document.getElementById('monday').checked &&
		!document.getElementById('Tuesday').checked &&
		!document.getElementById('Wednesday').checked &&
		!document.getElementById('Thursday').checked &&
		!document.getElementById('Friday').checked &&
		!document.getElementById('Saturday').checked &&
		!document.getElementById('Sunday').checked){

		alert('Selecciona al menos un dia');
	return false;

}else{
	return true;
}
}

//Luego de que se valida la información, es utilizada en la siguiente función
Client = function  (rideName, startFrom, end, description, departure, arrival,
	days, phone) {
	this.rideName = rideName;
	this.startFrom = startFrom;
	this.end = end;
	this.description = description;
	this.departure = departure;
	this.arrival = arrival;
	this.daysn = days;
	this.phone = phone;

	//Se crea el objeto ride para guardar en el LocalStorage.
	this.save = function() {
		var ride = {
			rideName,
			startFrom,
			end,
			description,
			departure,
			arrival,
			days,
			phone
		};

		//Por medio del key se obtiene toda la informacion que esta en el LocalStorage y si es null se crea [] para guardar
		var rides = JSON.parse(localStorage.getItem('rides'));
		if (rides == null) {
			rides = [];
			localStorage["rides"] = JSON.stringify(rides);
		}
		var equals = false;
		var ridesArray = [JSON.parse(localStorage["rides"])];//lee todo lo que hay en rides
		var array = [];
		var ridesArrayBackup = [];

		var quantity = ridesArray.length -1;
		if (quantity == 0) {

			for (var i = ridesArray.length - 1; i >= 0; i--) {
				array = ridesArray[i];
				for (var i = array.length - 1; i >= 0; i--) {
					var object = array[i];
					ridesArrayBackup[i] = object;

					if (ride.rideName == object.rideName) {
						if (phone == object.phone) {
							equals = true;
						}
					}
				}
				if (equals) {
					alert("El nombre de ese ride ya existe");
				}else{
					ridesArrayBackup.push(ride);
					localStorage["rides"] = JSON.stringify(ridesArrayBackup);
					alert("Registro exitoso");
				}
			}
		}
	};
},


rideSave = function () {
	var rideName = document.getElementById('rideName').value;
	var startFrom = document.getElementById('startFrom').value;
	var end = document.getElementById('end').value;
	var description = document.getElementById('description').value;
	var departure = document.getElementById('departure').value;
	var arrival = document.getElementById('arrival').value;

//verifica que halla algun dia marcado y lo guarda
	var daysn = [];
	var inputElements = document.getElementsByName('days');
	for(var i=0; inputElements[i]; ++i){
		if(inputElements[i].checked){
			daysn.push(inputElements[i].value);
		}
	}

	if (validarRideName() && validarStartFrom()
		&& validarEnd() && validarDescription()
		&& validarDeparture() && validarArrival()
		&& validarCheckbox()) {
		if (departure < arrival) {
			//Se crea una instancia y se envian los datos por parametros.
			var client = new Client(rideName, startFrom, end, description, departure, arrival,
				daysn, phone);
			client.save();//guarda el ride
			document.getElementById('rideName').value = "";
			document.getElementById('startFrom').value = "";
			document.getElementById('end').value = "";
			document.getElementById('description').value = "";
			document.getElementById('departure').value = "";
			document.getElementById('arrival').value = "";
		}else{
			alert("La hora de llegada debe ser mayor a la de salida");
		}
	}
}
